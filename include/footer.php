<footer>
    <div class="body_footer">
        <h5 class="title_footer">Do not hesitate to contact me <i class="fa fa-long-arrow-right"></i></h5>
        <p>If you want more information about me or my works, I invite you to get in touch via the multiple communication ways available.<br/>Thank you!</p><br/>
        <p><i class="fa fa-copyright"></i> Kevin Detrain - All rights reserved</p>
    </div><!---
 --><div class="info_footer">
        <h5 class="title_footer">Contact information</h5>
        <p>
            <a href="https://be.linkedin.com/pub/kevin-detrain/a9/b91/398">
            <i class="fa fa-linkedin-square"></i> kevin Detrain</a>
        </p>
        <p><a href="/Contact.html"><i class="fa fa-envelope-o"></i> Contact</a></p>        
        <p><a href="mailto:kevindetrain@gmail.com"><i class="fa fa-envelope"></i> kevindetrain@gmail.com</a></p>
    </div>
</footer>
<script>
</script>